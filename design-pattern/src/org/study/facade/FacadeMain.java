package org.study.facade;

import org.study.facade.system.Facade;

public class FacadeMain {

    public static void main(String[] args) {

        Facade facade = new Facade();

        facade.process();
    }
}
