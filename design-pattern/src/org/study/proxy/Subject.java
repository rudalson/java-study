package org.study.proxy;

public interface Subject {

    // less resource
    void action1();

    // many resource
    void action2();
}
