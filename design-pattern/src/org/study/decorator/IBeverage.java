package org.study.decorator;

public interface IBeverage {

    // 총가격
    int getTotalPrice();
}
