package org.study.command;

public interface Command extends Comparable<Command> {
    void execute();
}
