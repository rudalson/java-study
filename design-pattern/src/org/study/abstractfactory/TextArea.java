package org.study.abstractfactory;

public interface TextArea {
    public String getText();
}
