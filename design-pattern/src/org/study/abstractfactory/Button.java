package org.study.abstractfactory;

public interface Button {

    public void click();
}
