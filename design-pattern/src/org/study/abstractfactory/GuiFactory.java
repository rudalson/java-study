package org.study.abstractfactory;

public interface GuiFactory {

    public Button createButton();

    public TextArea createTextArea();

}
