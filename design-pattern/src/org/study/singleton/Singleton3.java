package org.study.singleton;

public enum Singleton3 {

    INSTANCE;

    public String getName() {
        return "Singleton";
    }
}
