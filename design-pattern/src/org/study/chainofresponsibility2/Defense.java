package org.study.chainofresponsibility2;

public interface Defense {

    void defense(Attack attack);
}
