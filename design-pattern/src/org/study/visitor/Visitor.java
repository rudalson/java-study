package org.study.visitor;

public interface Visitor {

    public void visit(Visitable visitable);
}
