package org.study.visitor;

public interface Visitable {
    void accept(Visitor visitor);
}
