package org.study.strategy;

public interface Weapon {
	void attack();
}
