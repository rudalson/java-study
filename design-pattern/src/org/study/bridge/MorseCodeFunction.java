package org.study.bridge;

public interface MorseCodeFunction {

    void dot();

    void dash();

    void space();
}
